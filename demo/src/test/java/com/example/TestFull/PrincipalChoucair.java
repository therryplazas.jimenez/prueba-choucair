package com.example.TestFull;

import com.example.demo.ScreenshotChoucairPage;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PrincipalChoucair {
    WebDriver d = new ChromeDriver();
    JavascriptExecutor js = (JavascriptExecutor) d;
    ScreenshotChoucairPage sc = new ScreenshotChoucairPage(d);

    Date date = new Date();
    DateFormat fechaHora = new SimpleDateFormat("ddMMyyy-HHmmss");
    String fecha = fechaHora.format(date);
    String name = "1. Home - ";


    @BeforeAll
    public static void setUpp() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
    }

    @BeforeEach
    public void setUp() {
        d.manage().window().maximize();
        d.get("https://www.choucairtesting.com/");
    }

    @Test
    public void Home() throws IOException, InterruptedException {

        d.findElement(By.xpath("//*[@id=\"slideshow\"]/div[2]/a")).click();
        sc.scrennShot(fecha, name);
        System.err.println("El boton no realiza ninguna accion.");
        Thread.sleep(2000);
        js.executeScript("window.scrollBy(0,1500)");
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id=\"content\"]/div/div/div/div/div/section[6]/div/div/div[1]/div/div/div/div/div/figure/a/img")).click();
        js.executeScript("window.scrollBy(0,-1500)");
        Thread.sleep(2000);
        fecha = fechaHora.format(new Date());
        name = "2. Industria - ";
        sc.scrennShot(fecha, name);
        js.executeScript("window.scrollBy(0,-1500)");
        d.findElement(By.xpath("//*[@id=\"content\"]/div/div/div/div/div/section[2]/div/div/div[1]/div/div/div[2]/div/div/figure/a/img")).click();
        Thread.sleep(2000);
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
        js.executeScript("window.scrollBy(0,-1500)");
        d.findElement(By.xpath("//*[@id=\"content\"]/div/div/div/div/div/section[2]/div/div/div[2]/div/div/div[2]/div/div/figure/a/img")).click();
        Thread.sleep(2000);
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
        js.executeScript("window.scrollBy(0,-1500)");
        d.findElement(By.xpath("//*[@id=\"content\"]/div/div/div/div/div/section[2]/div/div/div[3]/div/div/div[2]/div/div/figure/a/img")).click();
        Thread.sleep(2000);
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
        js.executeScript("window.scrollBy(0,-2500)");
        d.findElement(By.xpath("//*[@id=\"content\"]/div/div/div/div/div/section[2]/div/div/div[4]/div/div/div[2]/div/div/figure/a/img")).click();
        Thread.sleep(2000);
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
        js.executeScript("window.scrollBy(0,-3500)");
        d.findElement(By.xpath("//*[@id=\"content\"]/div/div/div/div/div/section[2]/div/div/div[5]/div/div/div[2]/div/div/figure/a/img")).click();
        Thread.sleep(2000);
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
        js.executeScript("window.scrollBy(0,-4500)");
        d.findElement(By.xpath("//*[@id=\"content\"]/div/div/div/div/div/section[2]/div/div/div[6]/div/div/div[2]/div/div/figure/a/img")).click();
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
        js.executeScript("window.scrollBy(0,1500)");
        Thread.sleep(2000);
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
        js.executeScript("window.scrollBy(0,1500)");
        Thread.sleep(2000);
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
    }

    @Test
    public void JobsRegistrationSuccessful() throws IOException, InterruptedException {

        sc.scrennShot(fecha, name);
        d.findElement(By.id("menu-item-550")).click();
        name = "3. Empleos - ";
        Thread.sleep(2000);
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
        js.executeScript("window.scrollBy(0,3500)");
        d.findElement(By.id("search_keywords")).sendKeys("Automatizador");
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
        d.findElement(By.id("search_location")).sendKeys("Bogota");
        d.findElement(By.xpath("//*[@id=\"content\"]/div/div/div/div/div/section[8]/div/div/div/div/div/div[3]/div/div/div/form/div[1]/div[4]/input")).click();
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id=\"content\"]/div/div/div/div/div/section[8]/div/div/div/div/div/div[3]/div/div/div/ul/li/a")).click();
        Thread.sleep(2000);
        js.executeScript("window.scrollBy(0,500)");
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
        d.findElement(By.xpath("//*[@id=\"post-10723\"]/div/div[2]/div[2]/input")).click();
        Thread.sleep(2000);
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
        d.findElement(By.id("nombre-completo")).sendKeys("Persona de prueba");
        d.findElement(By.id("correo-electronico")).sendKeys("prueba@gmail.com");
        d.findElement(By.id("celular-o-telefono-de-contacto")).sendKeys("3022850086");
        d.findElement(By.id("que-estudios-formales-tienes-o-en-que-semestre-te-encuentras-actualmente")).sendKeys("Especializacion");
        d.findElement(By.id("que-tiempo-de-experiencia-certificada-tienes-en-pruebas-o-en-desarrollo-de-softwaresi-aplica")).sendKeys("2 años");
        d.findElement(By.id("conoces-de-automatizacion-de-pruebas-te-gustaria-aprendersi-aplica")).sendKeys("Si");
        d.findElement(By.id("cual-es-tu-aspiracion-salarial")).sendKeys("2600000");
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
        js.executeScript("window.scrollBy(0,500)");
        d.findElement(By.id("si-eres-seleccionado-que-disponibilidad-de-tiempo-para-ingresar-tendrias")).sendKeys("Inmediato");
        d.findElement(By.id("mensaje-adicional")).sendKeys("Esto es una prueba automatizada.");
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
        js.executeScript("window.scrollBy(0,500)");
        d.findElement(By.xpath("//*[@id=\"post-10723\"]/div/div[2]/div[2]/div/form/p/input[1]")).click();
        Thread.sleep(5000);
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
    }

    @Test //@Disabled
    public void JobsRegisterFieldsEmpty() throws IOException, InterruptedException {

        sc.scrennShot(fecha, name);
        d.findElement(By.id("menu-item-550")).click();
        name = "3. Empleos campos vacios - ";
        Thread.sleep(2000);
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
        js.executeScript("window.scrollBy(0,3500)");
        d.findElement(By.id("search_keywords")).sendKeys("Automatizador");
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
        d.findElement(By.id("search_location")).sendKeys("Bogota");
        d.findElement(By.xpath("//*[@id=\"content\"]/div/div/div/div/div/section[8]/div/div/div/div/div/div[3]/div/div/div/form/div[1]/div[4]/input")).click();
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id=\"content\"]/div/div/div/div/div/section[8]/div/div/div/div/div/div[3]/div/div/div/ul/li/a")).click();
        Thread.sleep(2000);

        js.executeScript("window.scrollBy(0,500)");
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
        d.findElement(By.xpath("//*[@id=\"post-10723\"]/div/div[2]/div[2]/input")).click();
        Thread.sleep(2000);
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);

        js.executeScript("window.scrollBy(0,2500)");
        d.findElement(By.xpath("//*[@id=\"post-10723\"]/div/div[2]/div[2]/div/form/p/input[1]")).click();
        js.executeScript("window.scrollBy(0,-500)");
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
        Thread.sleep(2000);
        d.findElement(By.id("nombre-completo")).sendKeys(" ");

        js.executeScript("window.scrollBy(0,2500)");
        d.findElement(By.xpath("//*[@id=\"post-10723\"]/div/div[2]/div[2]/div/form/p/input[1]")).click();
        js.executeScript("window.scrollBy(0,-500)");
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
        Thread.sleep(2000);
        d.findElement(By.id("correo-electronico")).sendKeys(" ");

        js.executeScript("window.scrollBy(0,2500)");
        d.findElement(By.xpath("//*[@id=\"post-10723\"]/div/div[2]/div[2]/div/form/p/input[1]")).click();
        js.executeScript("window.scrollBy(0,-500)");
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
        Thread.sleep(2000);
        d.findElement(By.id("celular-o-telefono-de-contacto")).sendKeys(" ");

        js.executeScript("window.scrollBy(0,2500)");
        d.findElement(By.xpath("//*[@id=\"post-10723\"]/div/div[2]/div[2]/div/form/p/input[1]")).click();
        js.executeScript("window.scrollBy(0,-500)");
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
        Thread.sleep(2000);
        d.findElement(By.id("que-estudios-formales-tienes-o-en-que-semestre-te-encuentras-actualmente")).sendKeys(" ");

        js.executeScript("window.scrollBy(0,2500)");
        d.findElement(By.xpath("//*[@id=\"post-10723\"]/div/div[2]/div[2]/div/form/p/input[1]")).click();
        js.executeScript("window.scrollBy(0,-500)");
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
        Thread.sleep(2000);
        d.findElement(By.id("que-tiempo-de-experiencia-certificada-tienes-en-pruebas-o-en-desarrollo-de-softwaresi-aplica")).sendKeys(" ");

        js.executeScript("window.scrollBy(0,2500)");
        d.findElement(By.xpath("//*[@id=\"post-10723\"]/div/div[2]/div[2]/div/form/p/input[1]")).click();
        js.executeScript("window.scrollBy(0,-500)");
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
        Thread.sleep(2000);
        d.findElement(By.id("conoces-de-automatizacion-de-pruebas-te-gustaria-aprendersi-aplica")).sendKeys(" ");

        js.executeScript("window.scrollBy(0,2500)");
        d.findElement(By.xpath("//*[@id=\"post-10723\"]/div/div[2]/div[2]/div/form/p/input[1]")).click();
        js.executeScript("window.scrollBy(0,-500)");
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
        Thread.sleep(2000);
        d.findElement(By.id("cual-es-tu-aspiracion-salarial")).sendKeys(" ");

        js.executeScript("window.scrollBy(0,2500)");
        d.findElement(By.xpath("//*[@id=\"post-10723\"]/div/div[2]/div[2]/div/form/p/input[1]")).click();
        js.executeScript("window.scrollBy(0,-500)");
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
        Thread.sleep(2000);
        d.findElement(By.id("si-eres-seleccionado-que-disponibilidad-de-tiempo-para-ingresar-tendrias")).sendKeys(" ");

        js.executeScript("window.scrollBy(0,2500)");
        d.findElement(By.xpath("//*[@id=\"post-10723\"]/div/div[2]/div[2]/div/form/p/input[1]")).click();
        js.executeScript("window.scrollBy(0,-500)");
        fecha = fechaHora.format(new Date());
        sc.scrennShot(fecha, name);
        Thread.sleep(2000);
        d.findElement(By.id("mensaje-adicional")).sendKeys(" ");

        js.executeScript("window.scrollBy(0,2500)");
        d.findElement(By.xpath("//*[@id=\"post-10723\"]/div/div[2]/div[2]/div/form/p/input[1]")).click();
        Thread.sleep(3000);

    }

    @AfterEach
    public void exit() {
        d.close();
    }
}

