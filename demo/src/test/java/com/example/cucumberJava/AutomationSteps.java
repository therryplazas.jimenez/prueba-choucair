package com.example.cucumberJava;

import com.example.demo.ScreenshotChoucairPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

class AutomationSteps {
    WebDriver d = new ChromeDriver();
    JavascriptExecutor js = (JavascriptExecutor) d;
    ScreenshotChoucairPage sc = new ScreenshotChoucairPage(d);
    Date date = new Date();
    DateFormat fechaHora = new SimpleDateFormat("ddMMyyy-HHmmss");
    String fecha = fechaHora.format(date);
    String name = "4. ConsultAutomation - ";


    @Given("enter the url {string}")
    public void enterTheUrl(String arg0) {

        d.manage().window().maximize();
        d.get(arg0);
        throw new io.cucumber.java.PendingException();
    }

    @When("enter {string} and {string} select a vacancy.")
    public void enterAndSelectAVacancy(String arg0, String arg1) throws InterruptedException, IOException {

        js.executeScript("window.scrollBy(0,3500)");
        d.findElement(By.xpath(""));

        Thread.sleep(2000);
        sc.scrennShot(fecha, name);
        d.findElement(By.id("search_keywords")).sendKeys(arg0);
        d.findElement(By.id("search_location")).sendKeys(arg1);
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id=\"content\"]/div/div/div/div/div/section[8]/div/div/div/div/div/div[3]/div/div/div/ul/li[2]/a")).click();
        throw new io.cucumber.java.PendingException();
    }

    @When("enter {string} Lima and {string} select a vacancy.")
    public void enterLimaAndSelectAVacancy(String arg0, String arg1) throws InterruptedException, IOException {
        js.executeScript("window.scrollBy(0,3500)");
        d.findElement(By.xpath(""));

        Thread.sleep(2000);
        sc.scrennShot(fecha, name);
        d.findElement(By.id("search_keywords")).sendKeys(arg0);
        d.findElement(By.id("search_location")).sendKeys(arg1);
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id=\"content\"]/div/div/div/div/div/section[8]/div/div/div/div/div/div[3]/div/div/div/ul/li[1]/a")).click();
        throw new io.cucumber.java.PendingException();
    }


    @Then("the result is {string}")
    public void theResultIs(String arg0) {
        String urll = d.getCurrentUrl();
        if (arg0 == "https://www.choucairtesting.com/job/automatizador-choucair/" || arg0 == "https://www.choucairtesting.com/job/analista-de-pruebas-lima/") {
            System.out.println("Se redirecciono a la url correcta");
        } else {
            System.err.println("Las URLs no coincidieron");
            System.out.println("la ulr fue" + arg0);
        }

        throw new io.cucumber.java.PendingException();
    }



}

