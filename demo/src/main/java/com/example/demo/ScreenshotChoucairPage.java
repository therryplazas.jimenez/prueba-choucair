package com.example.demo;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ScreenshotChoucairPage {
    Date date = new Date();
    DateFormat fechaHora = new SimpleDateFormat("ddMMyyy-HHmmss");
    String fecha = fechaHora.format(date);
    String name;
    WebDriver d;
    String url = "Evidencia/";

    public ScreenshotChoucairPage(WebDriver d) {
        this.d = d;
    }

    public void scrennShot(String fecha, String name) throws IOException {

        File screenshot = ((TakesScreenshot) d).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshot, new File(url + name + fecha + ".png"));
    }

}
