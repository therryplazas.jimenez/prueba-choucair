Feature: As a User, i want to consult Automation vacancies.

  Background: A webPage
    Given enter the url "https://www.choucairtesting.com/empleos-testing/"

  Scenario: Consult test automation vacancy
    When enter "Automatizador" and "Bogota" select a vacancy.
    Then the result is "https://www.choucairtesting.com/job/automatizador-choucair/"

  Scenario: Consult test analyst vacancy
    When enter "Analista de Pruebas" Lima and "Lima Peru" select a vacancy.
    Then the result is "https://www.choucairtesting.com/job/analista-de-pruebas-lima/"

